## create an automation for sending friend requests and messaging
## hr recruiters in LinkedIn

from selenium import webdriver
import time
from selenium.webdriver.common.keys import Keys
import re
import sqlite3
import threading
import matplotlib.pyplot as plt
import numpy as np
 

class LinkedInBot():
    def __init__(self, email, password):
        self.browser = webdriver.Chrome()
        self.email = email
        self.password = password
        self.website = "https://www.linkedin.com/"        
        self.people_by_profession = {}
        self.keywords_by_profession = {}
        self.people_by_profession["developers"] = {}
        self.people_by_profession["cyber_people"] = {}
        self.people_by_profession["devops_people"] = {}
        self.people_by_profession["hr_people"] = {}
        self.people_by_profession["management"] = {}
        self.people_by_profession["others"] = {}
        self.keywords_by_profession["developers"] = ("developer","programmer", "software","data scientist","computer science","data analyst","machine learning","algorithm","deep learning","sull stack")
        self.keywords_by_profession["cyber_people"] = ("cyber","security","red team","blue team","soc","noc","pentester","penetration tester","malware researcher","reverse engineer","info sec")
        self.keywords_by_profession["devops_people"] = ("devops","virtualization","database","cloud", "information technology","network","dba","storage","solutions architect","solutions engineer","system administrator","it technician","automation engineer","it engineer","it manager","it team lead","infrastructure","data center architect")
        self.keywords_by_profession["hr_people"] = ("hr","recruiter","recruit","human resource","headhunter","headhunting","talent acquisition")
        self.keywords_by_profession["management"] = ("ceo","cto","cso","ciso","founder","vp","product manager","project manager")
        # first_tier_companies = ["facebook","google","oracle","twitter","mellanox"]
        # second_tier_companies = []
        # third_tier_companies = ["idf"]        
    
    def connect_db(self):
        self.db = LinkedInDB()

    def signIn(self):
        """ login to the website """

        self.browser.get(self.website)
        email_input = self.browser.find_element_by_id("login-email")
        password_input = self.browser.find_element_by_id("login-password")
        email_input.send_keys(self.email)
        password_input.send_keys(self.password)
        password_input.send_keys(Keys.ENTER)
        time.sleep(1)
    

    def get_people_by_profession(self,max_people=1000):
        """ get information on new people and save it to db  """
      
        self.signIn()
        browser = self.browser        
        table = self.db
        people_number = 0
        link_mynetwork = "https://www.linkedin.com/mynetwork/"
        browser.get(link_mynetwork)
        people = browser.find_elements_by_class_name("discover-person-card")
        while people_number < max_people:
            self.scroll_down()
            people = browser.find_elements_by_class_name("discover-person-card")
            people_number = people.__len__()
            print( people_number)
            
        
        for person in people:
            person_link = person.find_element_by_class_name("discover-person-card__image-link")
            person_link = person_link.get_attribute("href")
            person = person.text.splitlines()       
            if person.__len__() != 0:                 
                profession = self.check_profession(person[3])
                self.people_by_profession[profession][person[1]] = [person[3]]
                self.people_by_profession[profession][person[1]].append(person_link)            
                table.add_row(person[1],person[3],profession,person_link)

        table.commit_changes()
        create_bar_chart()
        
        ### show results 

        # self.print_results(max_people)

        
    def get_connections_stats(self):

        browser = self.browser
        base_url = "https://www.linkedin.com/search/results/people/?facetNetwork=%5B%22F%22%5D&origin=CLUSTER_EXPANSION"
        browser.get(base_url)
        total = browser.find_element_by_class_name("search-results__total")
        total = total.text
        max_people = int(re.sub("[^0-9]", "", total))
        people = browser.find_elements_by_class_name("search-entity")           
        page = 0        
        not_found = True 
        all_list = []
        while not_found:         
            page += 1
            url = base_url + "&page={}".format(page)         
            browser.get(url)
            try:
                found = browser.find_element_by_class_name("t-20")                
                found = found.text                    
                if found == 'No results found.':
                    not_found = False
            except:
                self.scroll_down()
                people = browser.find_elements_by_class_name("search-entity")    
                print(len(people))                    
                for person in people:                    
                    person = person.text.splitlines()                    
                    profession = self.check_profession(person[4])
                    self.people_by_profession[profession][person[1]] = [person[4]]
                    all_list.append([person[1],person[4]])

            

        
        ### show results 
        self.print_results(max_people)
        print("\n {}".format(all_list.__len__()))
                    

    def print_results(self,max_people):
        for profession in self.people_by_profession:
            amount_of_people = len(self.people_by_profession[profession])
            percent = (amount_of_people/max_people)*100
            print("profession: {0} has {1} people . {2}% \n".format(profession, amount_of_people , percent))
            print(self.people_by_profession[profession])
            print("\n\n")
    

    def check_profession(self, profession_in_title):
        """ check title for keywords in order to label the person's profession """

        profession_in_title = profession_in_title.lower()        
        keywords_by_profession = self.keywords_by_profession

        ## loop every profession and every keyword in that profession 
        for profession in keywords_by_profession:
            for keyword in keywords_by_profession[profession]:
                if keyword in profession_in_title:
                    return profession

        return "others"
        

    def connect(self,link):
        """ get a profile's link , connect and update db """
        browser = self.browser        
        browser.get(link)
        try:
            connect_btn = browser.find_element_by_class_name("pv-s-profile-actions--connect")
            connect_btn.click()
            btns = browser.find_element_by_class_name("send-invite__actions")
            confirm_btn = btns.find_element_by_class_name("ml1")
            confirm_btn.click()
        except Exception as e:
            print(e) 
        self.db.update_connection(link)
        self.db.commit_changes()



    def scroll_down(self):
        browser = self.browser
        SCROLL_PAUSE_TIME = 0.2
        # Get scroll height
        last_height = browser.execute_script("return document.body.scrollHeight")

        for i in range(50):
            # Scroll down to bottom
            browser.execute_script("window.scrollTo(0, document.body.scrollHeight);")

            # Wait to load page
            time.sleep(SCROLL_PAUSE_TIME)

            # Calculate new scroll height and compare with last scroll height
            new_height = browser.execute_script("return document.body.scrollHeight")
            if new_height == last_height:
                break
            last_height = new_height

       

class LinkedInDB():

    def __init__(self,db_name="LinkedInDB"):
        self.db = sqlite3.connect(db_name)
            
    def close_db(self):
        self.db.close()

    def commit_changes(self):
        self.db.commit()

    def create_people_by_profession_table(self):
        """ create a new tabe in the db with all new people collected"""

        cursor = self.db.cursor()
        cursor.execute(''' CREATE TABLE LinkedInPeople(link TEXT PRIMARY KEY, name TEXT,
                       job_title TEXT, job_classification TEXT , connected INTEGER, viewed INTEGER) ''')

    def add_row(self, name, job_title, job_classification, link, connected=0,viewed=0):
        cursor = self.db.cursor()
        try:            
            cursor.execute('''INSERT INTO LinkedInPeople(name, job_title, job_classification, link, connected, viewed)
                    VALUES(?,?,?,?,?,?)''' , (name, job_title, job_classification, link, connected, viewed))
        except Exception as e:
            print(e) 
    
    def update_connection(self, link):
        "update person after a connection request was sent"

        try:
            cursor = self.db.cursor()
            cursor.execute('''UPDATE LinkedInPeople 
            SET connected = 1, viewed=1 WHERE link = "{}"; '''.format(link))
        except Exception as e:
            print(e) 

    def execute_query(self,query):
        cursor = self.db.cursor()
        try:
            res = cursor.execute(query)
            res = res.fetchall()
            return res
        except Exception as e:
            print(e)


def get_configs(file_name="conf.config"):
    """ return configs from configuration file """

    with open(file_name) as conf:
        configs = conf.read()
    configs = configs.splitlines()
    return configs


def add_people(bot,people):
    bot.connect_db()
    bot.signIn()
    for person in people:
        link = person[0]
        bot.connect(link)
        print(person)
        

def get_list_from_db(job_classification="developers",amount_of_people=100,connection_state=0):
    """ fetch people from the DB """

    if connection_state == "*":
        if amount_of_people == "*":
            query = '''SELECT * FROM LinkedInPeople WHERE job_classification="{0}" ;'''.format(job_classification)
        else:
            query = '''SELECT * FROM LinkedInPeople WHERE job_classification="{0}" LIMIT {1} ;'''.format(job_classification,amount_of_people)
    else:
        if amount_of_people == "*":
            query = '''SELECT * FROM LinkedInPeople WHERE job_classification="{0}"  AND connected={1} ;'''.format(job_classification,connection_state)
        else:
            query = '''SELECT * FROM LinkedInPeople WHERE job_classification="{0}"  AND connected={2} LIMIT {1} ;'''.format(job_classification,amount_of_people,connection_state)


    try:    
        db = LinkedInDB()    
        db = db.db
        cursor = db.cursor()
        people = cursor.execute(query)
        people = people.fetchall()
        people_list = people 
    except Exception as e:
        print(e)
    return people_list



def get_current_db_stats():
    """ get stats on every profession"""

    stats_professions = {"developers":{"total":0,"connected":0},"cyber_people":{"total":0,"connected":0},"devops_people":{"total":0,"connected":0},"hr_people":{"total":0,"connected":0},"management":{"total":0,"connected":0},"others":{"total":0,"connected":0}}
    for profession in stats_professions:
        total = get_list_from_db(profession,"*","*")
        connected = get_list_from_db(profession,"*", 1)
        stats_professions[profession]["total"] = total.__len__()
        stats_professions[profession]["connected"] = connected.__len__()
    return stats_professions

def create_bar_chart():
    stats = get_current_db_stats()
    professions =  ("developers","cyber_people","devops_people","hr_people","management","others")
    pos = np.arange(len(professions))
    totals = []
    connected = []
    not_connected = []
    for profession in stats:
        totals.append(stats[profession]["total"])
        connected.append(stats[profession]["connected"]) 
        not_connected.append(stats[profession]["total"]-stats[profession]["connected"])

    plt.bar(pos,connected,color='blue',edgecolor='black')
    plt.bar(pos,not_connected,color='pink',edgecolor='black',bottom=connected)
    plt.xticks(pos, professions)
    plt.xlabel('Category', fontsize=16)
    plt.ylabel('Amount of people', fontsize=16)
    plt.title('Amount of people by category',fontsize=18)
    plt.show()

if __name__ == "__main__": 

    users = []
    bots = []
    configs = get_configs()
    
  
     
    # create bots according to the number of users written in the file and insert them to a list
    # for line_num in range(0,configs.__len__(),2):
    #     users.append((configs[line_num], configs[line_num+1]))

    # create a bot from every user and insert them to a list
    # for user in users:
    #     bot = LinkedInBot(user[0],user[1])
    #     bots.append(bot)
    
    # people = get_list_from_db("devops_people")
    # for bot in bots:    
    #     threading.Thread(target=add_people, args=(bot,people,)).start()


    # db.create_people_by_profession_table()    

    # bot = LinkedInBot(users[1][0],users[1][1])
    # bot.connect_db()
    # bot.get_people_by_profession(1500)

    # bot.get_connections_stats()
    ## refresh: browser.navigate().refresh();

    # db.close_db()